import * as http from "http";
import * as WebSocket from "ws";
import sendkeys from "sendkeys";
import { AddressInfo } from "ws";

const httpServer = http.createServer();
const wsServer = new WebSocket.Server({ server: httpServer });
wsServer.on("connection", socket => {
    socket.on("message", (msg: string) => {
        console.log(msg);
        var key = JSON.parse(msg).key;
        if (key == "{F5}" || key == "{F8}") {
            sendkeys(key);
        } else {
            console.log("Unknown key", key);
        }
    });
    console.log("Connected!")
});
httpServer.listen(process.env.PORT || 8999, () => {
    console.log(`Server started on port ${(httpServer.address() as AddressInfo).port}`);
});