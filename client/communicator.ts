import * as WebSocket from "ws";
import { Keys } from "./keyListener";
import { setInterval } from "timers";

export class Communicator {

    private socket: WebSocket;
    private keyPressCb: (key: string) => void;

    async connect(address: string): Promise<void> {
        // this.keyPressCb = cb;
        this.socket = new WebSocket(address);
        this.socket.onclose = () => {
            console.log("CLOSED!");
        };
        await new Promise((resolve, reject) => {
            this.socket.onopen = () => {
                console.log("Connected.");
                this.socket.onmessage = this.onMsg;
                setInterval(() => this.socket.ping(), 1000);
                resolve();
            };
            this.socket.onerror = () => {
                reject();
            }
        });
    }

    sendKey(key: Keys) {
        this.socket.send(JSON.stringify({ key: `\{${Keys[key]}\}` }));
    }

    private onMsg = (event: { data: WebSocket.Data; type: string; target: WebSocket }) => {
        console.log("RECEIVED", event.data);
        // if (typeof event.data == "string") {
        //     this.keyPressCb(JSON.parse(event.data).key);
        // }
    }
}