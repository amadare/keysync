import { Communicator } from "./communicator";
import { KeyListener, Keys } from "./keyListener";

// console.log(process.argv)
// var address = process.argv.find(a => a.startsWith("-a")).split(" ")[1];
// var keysString = process.argv.find(a => a.startsWith("-k")).split(" ")[1];
// var keys = Keys[keysString];

var address = 'ws://localhost:8999';
var keys = [Keys.F5];

var communicator = new Communicator();
var keyListener = new KeyListener();

for (let key of keys) {
    keyListener.register(key);
}
keyListener.start(e => {
    console.log(e);
    communicator.sendKey(e.rawcode);
});
communicator.connect(address);